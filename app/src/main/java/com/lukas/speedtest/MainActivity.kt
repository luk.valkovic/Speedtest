package com.lukas.speedtest

import android.Manifest
import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.jakewharton.rxbinding2.view.RxView
import com.patloew.rxlocation.RxLocation
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.activity_main.*
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import java.net.InetAddress
import okio.Okio
import io.reactivex.Observable
import java.io.File
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "MainActivity"
        private const val DOWNLOAD_FILE_SIZE = 1000 * 1000 * 200
        private const val PING_TIMEOUT = 3000
        private const val DOWNLOAD_TIME_LIMIT = 15L
    }

    private val disposables = CompositeDisposable()
    private val appSchedulers = AppSchedulers()
    private lateinit var token: String
    private var bestServer: Server? = null
    private var avgSpeed: Double? = null
    private lateinit var rxPermissions: RxPermissions
    private lateinit var rxLocation: RxLocation
    private var isTestRunning = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initButton()
    }

    @SuppressLint("MissingPermission")
    private fun initButton() {
        rxPermissions = RxPermissions(this)
        rxLocation = RxLocation(this)
        RxView.clicks(btn_start)
                .subscribeOn(appSchedulers.ui)
                .compose(rxPermissions.ensure(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION))
                .doOnNext { granted ->
                    if (granted) {
                        disposables += rxLocation
                                .location()
                                .lastLocation()
                                .subscribe {
                                    if (isTestRunning) {
                                        stopSpeedTest()
                                    } else {
                                        startSpeedTest(it)
                                    }
                                    isTestRunning = isTestRunning.not()
                                }
                    } else {
                        Toast.makeText(this, R.string.no_location, Toast.LENGTH_SHORT).show()
                    }
                }
                .subscribe()
    }

    private fun startSpeedTest(location: Location) {
        clearTexts()
        RxBus.listen(SpeedEvent::class.java)
                .observeOn(appSchedulers.ui)
                .subscribeBy(
                        onNext = { speedSubscriber.onNext(it) },
                        onError = { speedSubscriber.onError(it) },
                        onComplete = { speedSubscriber.onComplete() }
                )

        disposables += ApiHelper.api.retrieveTestToken()
                .subscribeOn(appSchedulers.network)
                .doOnNext { token = it.token }
                .concatMap { return@concatMap ApiHelper.api.fetchServers(it.token, location.latitude.toString(), location.longitude.toString()) }
                .concatMapIterable { it }
                .take(5)
                .doOnNext {
                    val before = System.currentTimeMillis()
                    InetAddress.getByName(it.url.drop(7)).isReachable(PING_TIMEOUT)
                    val after = System.currentTimeMillis()
                    it.latency = after - before
                }
                .doOnNext { findBestServer(it) }
                .flatMap { return@flatMap Observable.just(bestServer) }
                .observeOn(appSchedulers.ui)
                .take(1)
                .flatMap {
                    tv_latency.text = getString(R.string.latency, bestServer?.latency)
                    tv_server_name.text = getString(R.string.server, bestServer?.country, bestServer?.city)
                    return@flatMap Observable.just(bestServer)
                }
                .observeOn(appSchedulers.network)
                .flatMap {
                    disposables += Observable.timer(DOWNLOAD_TIME_LIMIT, TimeUnit.SECONDS)
                            .subscribeOn(appSchedulers.disk)
                            .observeOn(appSchedulers.ui)
                            .subscribeBy(
                                    onNext = {},
                                    onError = { Log.d(TAG, "timer error", it) },
                                    onComplete = { stopSpeedTest() }
                            )
                    return@flatMap ApiHelper.api.testDownload(token, "${it.url}/download", DOWNLOAD_FILE_SIZE)
                }

                .switchMap {
                    val file = File.createTempFile("speedtest", null, cacheDir)
                    val sink = Okio.buffer(Okio.sink(file))
                    sink.writeAll(it.body()?.source())
                    sink.close()
                    return@switchMap Observable.just(file)
                }
                .subscribeBy(
                        onNext = { },
                        onError = { Log.e(TAG, "subscribe speed test error", it) },
                        onComplete = { }
                )

    }

    private fun clearTexts() {
        btn_start.text = getString(R.string.stop)
        tv_server_name.text = getString(R.string.finding)
        tv_latency.text = getString(R.string.empty)
        tv_speed.text = getString(R.string.empty)
    }

    private fun stopSpeedTest() {
        btn_start.text = getString(R.string.start)
        speedSubscriber.onComplete()
        bestServer = null
        avgSpeed = null
        disposables.clear()
        isTestRunning = false
        cacheDir.deleteRecursively()
    }

    private fun findBestServer(server: Server) {
        if ((bestServer == null) or (server.latency < bestServer?.latency ?: Long.MAX_VALUE)) {
            bestServer = server
        }
    }

    private val speedSubscriber = object : Subscriber<SpeedEvent> {
        override fun onComplete() {
            tv_speed.text = getString(R.string.avg_speed, avgSpeed)
        }

        override fun onSubscribe(s: Subscription?) {
        }

        override fun onNext(t: SpeedEvent) {
            avgSpeed = t.avgSpeed
            if (t.complete) {
                tv_speed.text = getString(R.string.avg_speed, t.avgSpeed)
                stopSpeedTest()
            } else {
                tv_speed.text = getString(R.string.speed, t.speed)
            }
        }

        override fun onError(t: Throwable?) {
            Log.e(TAG, "onError speed subscriber", t)
        }

    }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }
}
