package com.lukas.speedtest

import okhttp3.MediaType
import okhttp3.ResponseBody
import okio.*

class DownloadResponseBody(val responseBody: ResponseBody?,
                           val listener: SpeedListener) : ResponseBody() {

    companion object {
        private const val DEFAULT_LONG_VALUE = -1L
    }

    private var bufferedSource: BufferedSource? = null

    interface SpeedListener {
        fun update(speed: Double, avgSpeed: Double, complete: Boolean)
    }

    override fun contentLength(): Long = responseBody?.contentLength() ?: DEFAULT_LONG_VALUE

    override fun contentType(): MediaType? = responseBody?.contentType()

    override fun source(): BufferedSource = bufferedSource ?: Okio.buffer(source(responseBody?.source()))

    private fun source(source: Source?): Source = object : ForwardingSource(source) {
        var totalRead = 0L
        val startTime = System.currentTimeMillis()
        var previousSinkTime = DEFAULT_LONG_VALUE
        var totalCount = 0

        override fun read(sink: Buffer?, byteCount: Long): Long {

            val downloadedSize = super.read(sink, byteCount)
            if (downloadedSize != DEFAULT_LONG_VALUE) {
                totalRead += downloadedSize
                totalCount++
            }
            val startInterval = System.currentTimeMillis()

            if (startInterval - previousSinkTime > 0 && totalCount.rem(25) == 0 || downloadedSize == DEFAULT_LONG_VALUE) {
                listener.update(
                        calculateSpeed(downloadedSize.toDouble(), previousSinkTime, startInterval),
                        calculateAVGSpeed(totalRead.toDouble(), startTime),
                        downloadedSize == DEFAULT_LONG_VALUE
                )
            }
            previousSinkTime = startInterval

            return downloadedSize
        }
    }

    private fun calculateSpeed(bytesRead: Double, startInterval: Long, endInterval: Long): Double {
        if (bytesRead == DEFAULT_LONG_VALUE.toDouble()) return (-1).toDouble()
        return (bytesRead / (125 * (endInterval - startInterval)))
    }

    private fun calculateAVGSpeed(bytesRead: Double, startTime: Long): Double = calculateSpeed(bytesRead, startTime, System.currentTimeMillis())
}