package com.lukas.speedtest

import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.internal.platform.Platform
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface SpeedtestApi {

    @POST("/api/v1/tokens")
    fun retrieveTestToken(): Observable<Token>

    @GET("/api/v2/servers")
    fun fetchServers(@Header("x-test-token") token: String,
                     @Query("latitude") latitude: String,
                     @Query("longitude") longitude: String
    ): Observable<List<Server>>

    @GET
    @Streaming
    fun testDownload(@Header("x-test-token") token: String, @Url url: String, @Query("size") size: Int): Observable<Response<ResponseBody>>
}

object ApiHelper {

    private val listener = object : DownloadResponseBody.SpeedListener {

        override fun update(speed: Double, avgSpeed: Double, complete: Boolean) {
            RxBus.publish(SpeedEvent(speed, avgSpeed, complete))
        }
    }

    private val logging = LoggingInterceptor.Builder()
            .loggable(BuildConfig.DEBUG)
            .setLevel(Level.BASIC)
            .log(Platform.INFO)
            .request("Request")
            .response("Response")
            .build()

    private val client = OkHttpClient.Builder()
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(logging)
            .addNetworkInterceptor { chain ->
                val originalResponse = chain.proceed(chain.request())
                if (originalResponse.header("Content-Type") == "application/octet-stream") {
                    originalResponse.newBuilder().body(DownloadResponseBody(originalResponse.body(), listener)).build()
                } else {
                    originalResponse.newBuilder().body(originalResponse.body()).build()
                }
            }
            .build()

    private val retrofit = Retrofit.Builder()
            .baseUrl("http://speedtest.ubncloud.com/")
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create()) // <- add this
            .client(client)
            .build()

    val api = retrofit.create(SpeedtestApi::class.java)
}

data class Token(
        val token: String,
        val ttl: Int
)

data class Server(
        val url: String,
        val latitude: Double,
        val longitude: Double,
        val provider: String,
        val city: String,
        val country: String,
        val countryCode: String,
        val speedMbps: Int,
        var latency: Long
)
