package com.lukas.speedtest

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AppSchedulers {
    val db = Schedulers.single()
    val disk = Schedulers.io()
    val network = Schedulers.io()
    val newThread = Schedulers.newThread()
    val ui = AndroidSchedulers.mainThread()
}
