package com.lukas.speedtest

data class SpeedEvent(
        val speed: Double,
        val avgSpeed: Double,
        val complete: Boolean
)